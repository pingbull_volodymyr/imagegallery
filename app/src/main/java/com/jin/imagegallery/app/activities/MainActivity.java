package com.jin.imagegallery.app.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.jin.imagegallery.app.R;

/**
 * Main activity that shows image grid.
 *
 * @author Volodymyr Yakymovych
 * @since 10 April 2014
 */
public class MainActivity extends FragmentActivity {

    /**
     * @param savedInstanceState {@link android.os.Bundle}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
