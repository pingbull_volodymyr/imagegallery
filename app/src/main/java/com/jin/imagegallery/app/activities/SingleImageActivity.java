package com.jin.imagegallery.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.jin.imagegallery.app.R;
import com.jin.imagegallery.app.models.Image;
import com.jin.imagegallery.app.models.ImageModel;

public class SingleImageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);

        Intent intent = getIntent();
        String imageId = intent.getStringExtra("imageId");
        ImageModel imageModel = new ImageModel();
        Image image = imageModel.getImageById(Integer.parseInt(imageId));

        // Set title
        TextView titleView = (TextView) findViewById(R.id.single_image_title);
        titleView.setText(image.getTitle());

        // Set image
        ImageView imageView = (ImageView) findViewById(R.id.single_image_image);
        int resourceId= getResources().getIdentifier(image.getSource(), "drawable", getPackageName());
        imageView.setImageResource(resourceId);
    }
}
