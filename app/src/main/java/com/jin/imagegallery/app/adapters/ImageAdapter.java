package com.jin.imagegallery.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.jin.imagegallery.app.R;
import com.jin.imagegallery.app.models.Image;

import java.util.ArrayList;

/**
 * @author Volodymyr Yakymovych
 * @since 10 April 2014
 */
public class ImageAdapter extends BaseAdapter {

    private Context _context;
    private ArrayList<Image> _images;

    /**
     * @param context {@link android.content.Context} needed to get some system services.
     * @param images  List of {@link com.jin.imagegallery.app.models.Image} objects.
     */
    public ImageAdapter(Context context, ArrayList<Image> images) {
        super();
        this._context = context;
        this._images = images;
    }

    /**
     * Override parent method
     *
     * @param position Current position in list.
     * @return null
     */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /**
     * @return Count of elements in list with {@link com.jin.imagegallery.app.models.Image} objects.
     */
    @Override
    public int getCount() {
        return _images.size();
    }

    /**
     * @param position Current position in list
     * @return Current position in list
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set appropriate image to each grid item.
     *
     * @param position    Current position in list.
     * @param convertView {@link android.view.View}.
     * @param parent      {@link android.view.ViewGroup}.
     * @return View of single image.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ImageView imageView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) _context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            imageView = (ImageView) inflater.inflate(R.layout.image_grid_item, parent, false);
        } else {
            imageView = (ImageView) convertView;
        }

        Image image = _images.get(position);
        int resourceId = _context.getResources().getIdentifier(image.getSource(), "drawable", _context.getPackageName());
        imageView.setImageResource(resourceId);
        imageView.setTag(image);

        return imageView;
    }
}
