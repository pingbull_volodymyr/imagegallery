package com.jin.imagegallery.app.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.jin.imagegallery.app.R;
import com.jin.imagegallery.app.activities.SingleImageActivity;
import com.jin.imagegallery.app.adapters.ImageAdapter;
import com.jin.imagegallery.app.models.Image;
import com.jin.imagegallery.app.models.ImageModel;

import java.util.ArrayList;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ImageGridFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ImageGridFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 * @author Volodymyr Yakymovych
 * @since 10 April 2014
 */
public class ImageGridFragment extends Fragment implements AdapterView.OnItemClickListener {

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ImageGridFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImageGridFragment newInstance() {
        ImageGridFragment fragment = new ImageGridFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ImageGridFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_image_grid, container, false);
        ImageModel imageModel = new ImageModel();
        ArrayList<Image> images = imageModel.getImages();
        ImageAdapter imageAdapter = new ImageAdapter(getActivity(), images);
        GridView gridView = (GridView) v.findViewById(R.id.image_grid);
        gridView.setAdapter(imageAdapter);
        gridView.setOnItemClickListener(this);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Image image = (Image) view.getTag();
        Intent intent = new Intent(getActivity(), SingleImageActivity.class);
        intent.putExtra("imageId", String.valueOf(image.getId()));
        getActivity().startActivity(intent);
    }
}
