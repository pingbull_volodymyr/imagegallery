package com.jin.imagegallery.app.models;

/**
 * @author Volodymyr Yakymovych
 * @since 10 April 2014
 */
public class Image {

    private int _id;
    private String _title;
    private String _source;

    /**
     * @return Item id
     */
    public int getId() {
        return _id;
    }

    /**
     * @param id Item id
     */
    public void setId(int id) {
        this._id = id;
    }

    /**
     * @return Image title
     */
    public String getTitle() {
        return _title;
    }

    /**
     * @param title Image title
     */
    public void setTitle(String title) {
        this._title = title;
    }

    /**
     * @return Image source
     */
    public String getSource() {
        return _source;
    }

    /**
     * @param source Image source
     */
    public void setSource(String source) {
        this._source = source;
    }
}
