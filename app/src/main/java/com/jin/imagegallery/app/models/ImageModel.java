package com.jin.imagegallery.app.models;

import java.util.ArrayList;

/**
 * @author Volodymyr Yakymovych
 * @since 10 April 2014
 */
public class ImageModel {

    /**
     * Just an emulator of something real.
     *
     * @return List of {@link com.jin.imagegallery.app.models.Image} objects.
     */
    public ArrayList<Image> getImages() {
        ArrayList<Image> images = new ArrayList<Image>();
        for (int i = 1; i < 10; i++) {
            Image image = new Image();
            image.setId(i);
            image.setTitle("Car " + String.valueOf(i));
            image.setSource("car" + String.valueOf(i));
            images.add(image);
        }

        return images;
    }

    /**
     * Another emulator of something real.
     *
     * @param imageId Image ID
     * @return {@link com.jin.imagegallery.app.models.Image}
     */
    public Image getImageById(int imageId) {
        Image image = new Image();
        image.setId(imageId);
        image.setTitle("Car " + String.valueOf(imageId));
        image.setSource("car" + String.valueOf(imageId));

        return image;
    }
}
