package com.jin.imagegallery.app.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * View class that is the same as {@link android.widget.ImageView}, but make images squared.
 *
 * @author Volodymyr Yakymovych
 * @since 10 April 2014
 */
public class SquareImageView extends ImageView {

    /**
     * @param context {@link android.content.Context}.
     */
    public SquareImageView(final Context context) {
        super(context);
    }

    /**
     * @param context      {@link android.content.Context}.
     * @param attributeSet {@link android.util.AttributeSet}.
     */
    public SquareImageView(final Context context, final AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /**
     * @param context      {@link android.content.Context}.
     * @param attributeSet {@link android.util.AttributeSet}.
     * @param defStyle     Some int.
     */
    public SquareImageView(final Context context, final AttributeSet attributeSet, final int defStyle) {
        super(context, attributeSet, defStyle);
    }

    /**
     * Sets height the same as width.
     *
     * @param widthMeasureSpec Width.
     * @param heightMeasureSpec Height.
     */
    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        setMeasuredDimension(width, width);
    }

    /**
     * Sets height the same as width.
     *
     * @param width Current width.
     * @param height Current height.
     * @param oldWidth Old width.
     * @param oldHeight Old height.
     */
    @Override
    protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, width, oldWidth, oldHeight);
    }
}